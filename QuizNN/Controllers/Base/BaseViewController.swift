//
//  BaseViewController.swift
//  NNGames
//
//  Created by Yaroslav Brekhunchenko on 6/15/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    public class var nameOfClass: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    
    //MARK: lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setup controller props
        navigationController?.isNavigationBarHidden = true
    }

}
