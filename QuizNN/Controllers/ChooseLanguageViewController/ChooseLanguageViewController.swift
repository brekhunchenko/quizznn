//
//  ChooseLanguageViewController.swift
//  QuizNN
//
//  Created by Yaroslav Brekhunchenko on 6/20/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

import UIKit

class ChooseLanguageViewController: BaseViewController {

    //MARK: ChooseLanguageViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    //MARK: Actions
    
    @IBAction func backButtonAction(_ sender: Any, forEvent event: UIEvent) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func languageButtonAction(_ sender: UIButton) {
        let languageSelected: Language = Language(rawValue: sender.tag)!
        PMIDataSource.defaultDataSource.language = languageSelected
        
        let vc: QuizGameViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(format:"QuizGameViewController_%@", languageSelected.prefixFromLanguage())) as! QuizGameViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
