//
//  QuizLevel.swift
//  QuizNN
//
//  Created by Yaroslav Brekhunchenko on 6/20/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

import UIKit
import SwiftyJSON

class QuizLevel: NSObject {

    var questions:[QuizQuestion] = []
    
    //MARK: Initializers
    
    init(dictionary: JSON, languagePrefix: String) {
        for questionJSON in dictionary["questions"].arrayValue {
            let question : QuizQuestion = QuizQuestion(dictionary: questionJSON, languagePrefix: languagePrefix)
            self.questions.append(question)
        }
        
        super.init()
    }
    
}
