//
//  QuizQuestion.swift
//  QuizNN
//
//  Created by Yaroslav Brekhunchenko on 6/20/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

import UIKit
import SwiftyJSON

class QuizQuestion: NSObject {
    
    var id: Int
    var isLeftAnswerIsCorrect: Bool
    var leftImageName: String
    var leftButtonImageName: String
    var rightImageName: String
    var rightButtonImageName: String
    var quizTitleImageName: String
    
    //MARK: Initializers
    
    init(dictionary: JSON, languagePrefix: String) {
        self.id = dictionary["id"].intValue
        self.isLeftAnswerIsCorrect = dictionary["isLeftAnswerIsCorrect"].boolValue
        self.leftImageName = String(format: "quiz_question_left_%d", self.id)
        self.leftButtonImageName = String(format: "quiz_question_left_btn_%d_%@", self.id, languagePrefix)
        self.rightImageName = String(format: "quiz_question_right_%d", self.id)
        self.rightButtonImageName = String(format: "quiz_question_right_btn_%d_%@", self.id, languagePrefix)
        self.quizTitleImageName = String(format: "quiz_question_%d_%@", self.id, languagePrefix)
        super.init()
    }
    
}
