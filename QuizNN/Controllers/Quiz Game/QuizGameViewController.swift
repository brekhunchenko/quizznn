//
//  QuizGameViewController.swift
//  QuizNN
//
//  Created by Yaroslav Brekhunchenko on 6/20/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

import UIKit
import SwiftyJSON

class QuizGameViewController: BaseViewController {

    @IBOutlet weak var quizQuestionImageView: UIImageView!
    @IBOutlet weak var leftAnswerImageView: UIImageView!
    @IBOutlet weak var leftAnswerButton: UIButton!
    @IBOutlet weak var rightAnswerImageView: UIImageView!
    @IBOutlet weak var rightAnswerButton: UIButton!
    @IBOutlet weak var questionIndexLabel: UILabel!
    @IBOutlet weak var timerCounterLabel: UILabel!
    
    private var seconds: Double = 30.0
    private var gameTimer: Timer!
    
    var level: QuizLevel!
    var currentQuestionIndex: Int = 0 {
        didSet {
            self.updateUIForQuestionWithIndex(currentQuestionIndex)
        }
    }
    var numberOfCorrectAnswers: Int = 0
    
    //MARK: UIViewController Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupLevel()
        
        self.updateUIForQuestionWithIndex(currentQuestionIndex)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.beginTimerUpdates()
    }

    //MARK: Setup
    
    private func setupLevel() {
        let languagePrefix: String = PMIDataSource.defaultDataSource.language.prefixFromLanguage()
        let fileName : String = String(format: "quiz_\(languagePrefix)")
        let file = Bundle.main.url(forResource: fileName, withExtension: "json")
        let data = try! Data(contentsOf: file!)
        let json = try! JSON(data: data)
        
        self.level = QuizLevel(dictionary: json, languagePrefix: languagePrefix)
    }
    
    //MARK: Timer
    
    private func beginTimerUpdates() {
        gameTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    @objc private func timerAction() {
        self.seconds -= 0.1
        self.timerCounterLabel.text = "\(Int(self.seconds))"
        
        if self.seconds <= 0 {
            self.showLoseScreen()
            gameTimer.invalidate()
        }
    }
    
    //MARK: Game
    
    func updateUIForQuestionWithIndex(_ questionIndex: Int) {
        let question:QuizQuestion = level.questions[self.currentQuestionIndex]
        self.leftAnswerButton.setImage(UIImage(named: question.leftButtonImageName), for: .normal)
        self.rightAnswerButton.setImage(UIImage(named: question.rightButtonImageName), for: .normal)
        self.quizQuestionImageView.image = UIImage(named: question.quizTitleImageName)
        self.leftAnswerImageView.image = UIImage(named: question.leftImageName)
        self.rightAnswerImageView.image = UIImage(named: question.rightImageName)
        self.questionIndexLabel.text = "\(self.currentQuestionIndex + 1)/\(self.level.questions.count)"
    }
    
    func processAnswerSelected(isLeft: Bool) {
        let question: QuizQuestion = level.questions[self.currentQuestionIndex]
        let correct: Bool = question.isLeftAnswerIsCorrect == isLeft
        if correct {
            self.numberOfCorrectAnswers = self.numberOfCorrectAnswers + 1
        }
        
        if self.numberOfCorrectAnswers == 3 {
            self.showWinScreen()
        } else {
            if self.currentQuestionIndex == self.level.questions.count - 1 {
                self.showLoseScreen()
            } else {
                self.currentQuestionIndex = self.currentQuestionIndex + 1
            }
        }
    }
    
    //MARK: Actions
    
    @IBAction func leftAnswerButtonAction(_ sender: Any) {
        self.processAnswerSelected(isLeft: true)
    }
    
    @IBAction func rightAnswerButtonAction(_ sender: Any) {
        self.processAnswerSelected(isLeft: false)
    }
    
    //MARK: Route
    
    private func showLoseScreen() {
        gameTimer.invalidate()

        let vc: LoseViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(format:"LoseViewController_%@", PMIDataSource.defaultDataSource.language.prefixFromLanguage())) as! LoseViewController
                let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    private func showWinScreen() {
        gameTimer.invalidate()
        
        let vc: WinTextViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(format:"WinTextViewController_%@", PMIDataSource.defaultDataSource.language.prefixFromLanguage())) as! WinTextViewController
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        self.navigationController?.pushViewController(vc, animated: false)
    }
}
