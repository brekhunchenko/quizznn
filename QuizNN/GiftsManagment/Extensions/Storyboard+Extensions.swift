//
//  Storyboard+Extensions.swift
//  NNGames
//
//  Created by Yaroslav Brekhunchenko on 6/21/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    static func giftManagmentStoryBoard() -> UIStoryboard {
        return UIStoryboard(name: "HostessManagment", bundle: nil)
    }
    
}
